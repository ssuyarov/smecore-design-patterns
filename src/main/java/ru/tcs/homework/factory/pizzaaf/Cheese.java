package ru.tcs.homework.factory.pizzaaf;

public interface Cheese {
	public String toString();
}
