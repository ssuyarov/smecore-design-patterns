package ru.tcs.homework.factory.pizzaaf;

public interface Sauce {
	public String toString();
}
