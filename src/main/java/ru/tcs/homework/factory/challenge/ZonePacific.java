package ru.tcs.homework.factory.challenge;

public class ZonePacific extends Zone {
	public ZonePacific() {
		displayName = "US/Pacific";
		offset = -8;
	}
}