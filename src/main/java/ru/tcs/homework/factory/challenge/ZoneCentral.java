package ru.tcs.homework.factory.challenge;

public class ZoneCentral extends Zone {
	public ZoneCentral() {
		displayName = "US/Central";
		offset = -6;
	}
}