package ru.tcs.workshop.observer.suyarov;

import static java.lang.Thread.sleep;
import static ru.tcs.workshop.observer.suyarov.ButtonsBuilder.getButtons;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import ru.tcs.workshop.observer.CurrentConditionsDisplay;
import ru.tcs.workshop.observer.ForecastDisplay;
import ru.tcs.workshop.observer.HeatIndexDisplay;
import ru.tcs.workshop.observer.StatisticsDisplay;
import ru.tcs.workshop.observer.WeatherStation;

public class FuckingApplication extends Application {
    private static final WeatherStation weatherStation = new WeatherStation();

    private final FlowPane pane = new FlowPane();
    private Boolean active = true;
    @Override
    public void start(Stage primaryStage) throws Exception {
        new Thread(()->{
            while (active) {
                weatherStation.updateMeasurements();
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        pane.getChildren().addAll(getButtons(weatherStation,
            new CurrentConditionsDisplay(),
            new HeatIndexDisplay(),
            new ForecastDisplay(),
            new StatisticsDisplay()
        ));
        primaryStage.setScene(new Scene(pane, 300, 300));
        primaryStage.show();

    }

    @Override
    public void stop() throws Exception {
        active = false;
        super.stop();
    }

    public static void main(String[] args) throws InterruptedException {
        launch(args);
    }
}
