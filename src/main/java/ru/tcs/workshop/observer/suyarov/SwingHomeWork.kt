package ru.tcs.workshop.observer.suyarov

import javafx.scene.Node
import javafx.scene.control.Button
import ru.tcs.workshop.observer.ObservableInterface
import ru.tcs.workshop.observer.ObserverInterface


object ButtonsBuilder {
    @JvmStatic
    fun getButtons(observable: ObservableInterface, vararg observers: ObserverInterface): List<Node> {
        val map = mutableMapOf<ObserverInterface, Boolean>()
        return observers.map {
            Button(it.javaClass.simpleName).apply {
                setOnAction { _ ->
                    if (map[it] == true) observable.removeObserver(it)
                    else observable.registerObserver(it)
                    map[it] = !(map[it] ?: false)
                }
            }
        }
    }
}