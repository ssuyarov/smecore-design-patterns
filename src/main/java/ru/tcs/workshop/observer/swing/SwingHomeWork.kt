package ru.tcs.workshop.observer.swing

import ru.tcs.workshop.observer.WeatherStation


/**
 * TODO Homework Star: Make 4 buttons. Click on each of them subscribes/unsubscribes display
 * */
fun main() {

    val weatherStation = WeatherStation()
    // write your code here
    while (true) {
        weatherStation.updateMeasurements()
        Thread.sleep(2000)
    }
}
