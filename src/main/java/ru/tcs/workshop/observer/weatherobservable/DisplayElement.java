package ru.tcs.workshop.observer.weatherobservable;

public interface DisplayElement {
	public void display();
}
