package ru.tcs.workshop.observer

import kotlin.random.Random

fun main() {

    val weatherStation = WeatherStation()
    weatherStation.registerObserver(CurrentConditionsDisplay())
    weatherStation.registerObserver(StatisticsDisplay())
    weatherStation.registerObserver(ForecastDisplay())
    weatherStation.registerObserver(HeatIndexDisplay())
    while (true) {
        weatherStation.updateMeasurements()
        Thread.sleep(2000)
    }
}

class HeatIndexDisplay : ObserverInterface, DisplayElement {
    private var heatIndex = 0.0

    override fun display() {
        println("Heat index is $heatIndex")
    }
    override fun update(temperature: Double?, humidity: Double?, pressure: Double?) {
        if (temperature == null || humidity == null) return
        heatIndex = computeHeatIndex(temperature, humidity)
        display()
    }

    /**
     * https://www.wickedlysmart.com/headfirstdesignpatterns/heatindex.txt
     */
    private fun computeHeatIndex(t: Double, rh: Double): Double {
        return 16.923 + 0.185212 * t + 5.37941 * rh - 0.100254 * t * rh +
            0.00941695 * (t * t) + 0.00728898 * (rh * rh) +
            0.000345372 * (t * t * rh) - 0.000814971 * (t * rh * rh) + 0.0000102102 * (t * t * rh * rh) - 0.000038646 * (t * t * t) + 0.0000291583 * (rh * rh * rh) + 0.00000142721 * (t * t * t * rh) +
            0.000000197483 * (t * rh * rh * rh) - 0.0000000218429 * (t * t * t * rh * rh) + 0.000000000843296 * (t * t * rh * rh * rh) - 0.0000000000481975 * (t * t * t * rh * rh * rh)
    }
}

class ForecastDisplay : ObserverInterface, DisplayElement {
    private var currentPressure:Double = 0.0
    private var lastPressure: Double = 0.0
    override fun display() {
        print("Forecast: ")
        when {
            currentPressure > lastPressure -> println("Improving weather on the way!")
            currentPressure == lastPressure -> println("More of the same")
            currentPressure < lastPressure -> println("Watch out for cooler, rainy weather")
        }
    }

    override fun update(temperature: Double?, humidity: Double?, pressure: Double?) {
        this.lastPressure = currentPressure
        this.currentPressure = pressure ?: currentPressure
        display()
    }
}

/**
 * TODO Homework: optimize it
 * */
class StatisticsDisplay : ObserverInterface, DisplayElement {
    private var temperatures: MutableList<Double> = mutableListOf()

    override fun display() {
        println("Avg/Max/Min temperature: ${temperatures.average()}, ${temperatures.max()}, ${temperatures.min()}")
    }

    override fun update(temperature: Double?, humidity: Double?, pressure: Double?) {
        temperature?.let { this.temperatures.add(it) }
        display()
    }
}

class CurrentConditionsDisplay : ObserverInterface, DisplayElement {
    private var temperature: Double? = null
    private var humidity: Double? = null
    private var pressure: Double? = null

    override fun display() {
        println("Current conditions: ${temperature}F degrees and $humidity humidity")
    }

    override fun update(temperature: Double?, humidity: Double?, pressure: Double?) {
        this.temperature = temperature
        this.humidity = humidity
        this.pressure = pressure
        display()
    }
}

interface DisplayElement {
    fun display()
}


class WeatherStation : ObservableInterface {
    private val observers = mutableListOf<ObserverInterface>()
    private var temperature: Double? = null
    private var humidity: Double? = null
    private var pressure: Double? = null

    override fun registerObserver(observer: ObserverInterface) {
        observers.add(observer)
    }

    override fun removeObserver(observer: ObserverInterface) {
        observers.removeAt(observers.indexOf(observer))
    }

    override fun notifyObservers() {
        observers.onEach { it.update(temperature, humidity, pressure) }
    }

    fun updateMeasurements() {
        this.temperature = Random.Default.nextDouble(60.0, 100.0)
        this.humidity = Random.Default.nextDouble(60.0, 100.0)
        this.pressure = Random.Default.nextDouble(10.0, 40.0)
        println("|")
        notifyObservers()
    }
}

interface ObservableInterface {
    fun registerObserver(observer: ObserverInterface)
    fun removeObserver(observer: ObserverInterface)
    fun notifyObservers()
}

interface ObserverInterface {
    fun update(temperature: Double?, humidity: Double?, pressure: Double?)
}
