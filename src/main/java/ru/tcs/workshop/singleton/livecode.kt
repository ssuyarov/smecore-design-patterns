package ru.tcs.workshop.singleton

import java.lang.Thread.sleep
import java.math.BigDecimal
import kotlin.concurrent.thread

fun main() {
    val singletonLinks = mutableListOf<UglySingleton>()
    listOf(
        thread{ singletonLinks.add(UglySingleton.getInstance()) },
            thread{ singletonLinks.add(UglySingleton.getInstance()) },
            thread{ singletonLinks.add(UglySingleton.getInstance()) },
            thread{ singletonLinks.add(UglySingleton.getInstance()) },
            thread{ singletonLinks.add(UglySingleton.getInstance()) },
            thread{ singletonLinks.add(UglySingleton.getInstance()) },
            thread{ singletonLinks.add(UglySingleton.getInstance()) },
            thread{ singletonLinks.add(UglySingleton.getInstance()) },
            thread{ singletonLinks.add(UglySingleton.getInstance()) },
            thread{ singletonLinks.add(UglySingleton.getInstance()) }
    ).forEach {
        it.join()
    }

    singletonLinks.forEach { current ->
        println(current)
        singletonLinks.forEach {
            assert(it == current)
            assert(it === current)
        }
    }
}

class NewSingleton private constructor(){
    val name: String
    val value: BigDecimal
    init {
        name = "New____Singleton"
        value = BigDecimal.TEN
    }
    override fun toString(): String {
        return ("NewSingleton{name='$name', value=$value}${this.hashCode()} ${super.hashCode()}")
    }

    companion object {
        private val inst by lazy {
            NewSingleton()
        }
        fun getInstance(): NewSingleton{
            return inst
        }
    }
}

fun fromDynamicClassLoader(): Any? {
    return Class.forName(
        "ru.tcs.workshop.singleton.SimpleSingleton",
        true,
        DynamicClassOverloader(arrayOf("./build/classes/java/main"))
    ).let {
        it.getDeclaredMethod("getInstance").invoke(it)
    }
}

object KotlinSingleton{
    val name: String
    val value: BigDecimal
    init {
        name = "KOTLINE_SINGLETON"
        value = BigDecimal.TEN
    }
    override fun toString(): String {
        return ("KotlinSingleton{name='$name', value=$value}${this.hashCode()} ${super.hashCode()}")
    }
}

data class UglySingleton private constructor(
    var name: String = "UglySingleton",
    var value: Double = 0.51
) {
    init {
        sleep(1000)
    }
    companion object {
        private var inst: UglySingleton? = null
        fun getInstance() = inst ?: UglySingleton().also { inst = it }
    }
    override fun toString(): String {
        return ("UglySingleton{name='${name}', value=${value}}${this.hashCode()} ${super.hashCode()}")
    }
}

/**
open class Super {
inner class Inner {
object SomeObject // not allowed
}
}
object Some : Super.Inner()
 */