package ru.tcs.workshop.singleton;

import java.util.Objects;

public class SimpleSingleton {

    private volatile static SimpleSingleton instance;
//    volatile private static SimpleSingleton instance;
//    private  static volatile SimpleSingleton instance;

    private String name = "SimpleSingleton";
    private Double value = 15.11;

    private SimpleSingleton() {
        try {
            Thread.sleep(1000L);
            System.out.println("comppleted");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static SimpleSingleton getInstance() {
        if (instance == null) {
            synchronized (SimpleSingleton.class) {
                if (instance == null) {
                    instance = new SimpleSingleton();
                }
            }
        }
        return instance;
    }

    public static void setInstance(SimpleSingleton instance) {
        SimpleSingleton.instance = instance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SimpleSingleton that = (SimpleSingleton) o;
        return Objects.equals(name, that.name) &&
            Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, value);
    }

    @Override
    public String toString() {
        return "SimpleSingleton{" +
            "name='" + name + '\'' +
            ", value=" + value +
            '}'
            + this.hashCode() + ' ' + super.hashCode();
    }
}
