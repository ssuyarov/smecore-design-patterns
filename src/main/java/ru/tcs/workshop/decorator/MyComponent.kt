package ru.tcs.workshop.decorator

object MyComponent {
    fun <T> doSomething(list: List<T>) where T : Figure, T : Area {
        list.forEach {
            it.area()
            it.lineform
        }
    }
}