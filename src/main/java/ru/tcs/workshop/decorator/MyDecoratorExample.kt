package ru.tcs.workshop.decorator

import kotlin.math.PI
import kotlin.math.pow

fun main() {
    val figures = ioarejfainrppadr()
    println(AreaCalculator.sumAreas(figures))

    FigureRendere.render(figures)
}

private fun ioarejfainrppadr(): List<ConcreteFigure> {
    return mutableListOf(
        Square(1.0),
        Triangle(1.0),
        Square(2.0),
        BlueFigure(Circle(3.0))
    )
}

class BlueFigure(
    val figure: ConcreteFigure
) : ConcreteFigure() {
    override val lineform: String
        get() = "<blue>${figure.lineform}</blue>"

    override fun area(): Double {
        return figure.area()
    }

    override fun perimetr() {
        return figure.perimetr()
    }
}

abstract class ConcreteFigure : Figure(), Area, Perimetr
interface Perimetr {
    fun perimetr()
}
object AreaCalculator {
    fun sumAreas(figures: List<Area>): Double {
        return figures.sumByDouble { it.area() }
    }

}
interface Area {
    fun area(): Double
}

class Square(
    val sideLength: Double
) : ConcreteFigure() {
    override val lineform = "[ _ ]"
    override fun area(): Double {
        return sideLength.pow(2)
    }

    override fun perimetr() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

class Circle(
    val radius: Double
) : ConcreteFigure()  {
    override val lineform: String
        get() = "( )"

    override fun area(): Double {
        return  PI * radius.pow(2)
    }

    override fun perimetr() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

class Triangle(
    val sideLength: Double
) : ConcreteFigure()  {
    override val lineform: String
        get() = """/_\"""

    override fun perimetr() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun area(): Double {
        return sideLength.pow(2) / 2
    }
}