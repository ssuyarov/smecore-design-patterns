package ru.tcs.workshop.decorator

class RedFigure(
    private val figure: ConcreteFigure
) : ConcreteFigure(), Area by figure, Perimetr by figure {
    override val lineform: String
        get() = "<red>${figure.lineform}</red>"
}