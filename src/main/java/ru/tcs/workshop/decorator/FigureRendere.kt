package ru.tcs.workshop.decorator

object FigureRendere {
    fun render(list: List<Figure>) {
        println(list.map { it.lineform }.joinToString(","))
    }
}
