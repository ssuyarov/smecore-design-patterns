package ru.tcs.workshop.factory

fun main() {
    listOf(MoscowStore(), StPiterStore()).forEach {
        println(it.orderPizza("cheese"))
    }
}


class MoscowStore : PizzaStore(MoscowIngridientFactory())
class StPiterStore : PizzaStore(StPiterIngridientFactory())

abstract class PizzaStore(
    val factory: IngredientFactory
) {
    fun orderPizza(type: String) = createPizza(type).apply {
        prepare()
        bake()
        cut()
        box()
    }
    fun createPizza(order: String)  = when (order) {
        "cheese" -> CheesePizza(factory)
        "veggies" -> VeggiePizza(factory)
        else -> throw RuntimeException()
    }
}

/**
 * +++++++++ У нас есть 500 РАЗЛИЧНЫХ пиццерий
 * +++++++++ У каждой пицерии РАЗНОЕ меню пиццы
 * +++++++++ У каждой пиццы РАЗНЫЙ НАБОР ИНГРИДИЕНТОВ и СПОСОБ ГОТОВКИ
 * НО!
 * +++++++++ Некоторые ингридиенты принадлежат ОДНОМУ КЛАССУ
 * +++++++++ Все пиццы относятся подаются, упаковываются, нарезаются ОДИНАКОВО
 * +++++++++ Все пицерии принимают заказы ОДИНАКОВО
 *
 * */
class StPiterIngridientFactory : IngredientFactory {
    override fun createDough() = ADough()
    override fun createSauce() = CSauce()
    override fun createPeperony() = APeperony()
    override fun createClams() = BClams()
    override fun createVeggies() = listOf<AVeggies>()
    override fun createToppings() = listOf<CTopping>()
}
class MoscowIngridientFactory : IngredientFactory {
    override fun createDough() = BDough()
    override fun createSauce() = BSauce()
    override fun createPeperony() = CPeperony()
    override fun createClams() = BClams()
    override fun createVeggies() = listOf<BVeggies>()
    override fun createToppings() = listOf<BTopping>()
}

interface Dough
interface Sauce
interface Peperony
interface Clams
interface Veggies
interface Topping
class ADough:Dough; class BDough:Dough; class CDough:Dough
class ASauce:Sauce; class BSauce:Sauce; class CSauce:Sauce
class APeperony:Peperony; class BPeperony:Peperony; class CPeperony:Peperony
class AClams:Clams; class BClams:Clams; class CClams:Clams
class AVeggies:Veggies; class BVeggies:Veggies; class CVeggies:Veggies
class ATopping:Topping; class BTopping:Topping; class CTopping:Topping
interface IngredientFactory {
    fun createDough():Dough
    fun createSauce():Sauce
    fun createPeperony():Peperony
    fun createClams():Clams
    fun createVeggies(): List<Veggies>
    fun createToppings(): List<Topping>
}
class CheesePizza(
    private val factory: IngredientFactory
) : Pizza("Cheese Pizza"){
    lateinit var dough: Dough
    lateinit var sauce: Sauce
    lateinit var toppings: List<Topping>
    override fun prepare() {
        dough = factory.createDough()
        sauce = factory.createSauce()
        toppings = factory.createToppings()
    }
}

class VeggiePizza(
    private val factory: IngredientFactory
) : Pizza("Veggie Pizza") {

    lateinit var dough: Dough
    lateinit var sauce: Sauce
    lateinit var veggies: List<Veggies>

    override fun prepare() {
        dough = factory.createDough()
        sauce = factory.createSauce()
        veggies = factory.createVeggies()
    }
}

abstract class Pizza(
    private val name: String
) {
    abstract fun prepare()
    fun bake() = println("Baking $name")
    fun cut() = println("Cutting $name")
    fun box() = println("Boxing $name")

    override fun toString() = "---- $name ----"
}
