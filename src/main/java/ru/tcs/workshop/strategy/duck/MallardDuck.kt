package ru.tcs.workshop.strategy.duck

import ru.tcs.workshop.strategy.behav.ExoticEatBeahav
import ru.tcs.workshop.strategy.behav.FlyWithWings
import ru.tcs.workshop.strategy.behav.NoRunBehav
import ru.tcs.workshop.strategy.behav.SquickBehav

class MallardDuck : Duck(
    eatBehav = ExoticEatBeahav(),
    flyBehavior = FlyWithWings(),
    runBehavior = NoRunBehav(),
    quackBehavior = SquickBehav()
) {
    override fun display() {
        println("I'm MallardDuck duck")
    }

    override fun swim() {
        println(">>>>>>>>>>>>>>>>>>")
    }

}
