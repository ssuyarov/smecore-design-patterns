package ru.tcs.workshop.strategy.duck

import ru.tcs.workshop.strategy.behav.FlyWithWings
import ru.tcs.workshop.strategy.behav.NoEatBehav
import ru.tcs.workshop.strategy.behav.NoRunBehav
import ru.tcs.workshop.strategy.behav.SquickBehav

class RedheadDuck : Duck(
    eatBehav = NoEatBehav(),
    flyBehavior = FlyWithWings(),
    runBehavior = NoRunBehav(),
    quackBehavior = SquickBehav()
) {
    override fun display() {
        println("I'm RedheadDuck")
    }

    override fun swim() {
        println("<<<<<<<<<<<<<<<<<<")
    }

}
