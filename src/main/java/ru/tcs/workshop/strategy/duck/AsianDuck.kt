package ru.tcs.workshop.strategy.duck

import ru.tcs.workshop.strategy.behav.ExoticEatBeahav
import ru.tcs.workshop.strategy.behav.FastRunBehav
import ru.tcs.workshop.strategy.behav.FlyWithWings
import ru.tcs.workshop.strategy.behav.SilentQuackBehav

class AsianDuck: Duck(
    eatBehav = ExoticEatBeahav(),
    flyBehavior = FlyWithWings(),
    runBehavior = FastRunBehav(),
    quackBehavior = SilentQuackBehav()
) {
    override fun display() {
        println("OAENDWO@#*!)(")
    }

    override fun swim() {
        println("<<<<_________<<<<________>>>>-_______")
    }

}