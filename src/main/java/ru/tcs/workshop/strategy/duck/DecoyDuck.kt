package ru.tcs.workshop.strategy.duck

import ru.tcs.workshop.strategy.behav.NoEatBehav
import ru.tcs.workshop.strategy.behav.NoFlyBehav
import ru.tcs.workshop.strategy.behav.NoRunBehav
import ru.tcs.workshop.strategy.behav.SilentQuackBehav

class DecoyDuck : Duck(
    flyBehavior = NoFlyBehav(),
    runBehavior = NoRunBehav(),
    eatBehav = NoEatBehav(),
    quackBehavior = SilentQuackBehav()
) {
    override fun display() {
        println("[__________]")
    }

    override fun swim() {
        println("||")
    }
}
