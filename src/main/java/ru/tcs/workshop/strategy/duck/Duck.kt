package ru.tcs.workshop.strategy.duck

import ru.tcs.workshop.strategy.behav.EatBehavior
import ru.tcs.workshop.strategy.behav.FlyBehavior
import ru.tcs.workshop.strategy.behav.QuackBehavior
import ru.tcs.workshop.strategy.behav.RunBehavior

abstract class Duck(
    private val flyBehavior: FlyBehavior,
    private val quackBehavior: QuackBehavior,
    private val runBehavior: RunBehavior,
    private val eatBehav: EatBehavior
) {
    abstract fun display()
    abstract fun swim()

    fun quack() {
        quackBehavior.quack()
    }

    fun fly() {
        flyBehavior.fly()
    }

    fun run() {
        runBehavior.run()
    }

    fun eat(){
        eatBehav.eat()
    }
}
