package ru.tcs.workshop.strategy.duck

import ru.tcs.workshop.strategy.behav.NoEatBehav
import ru.tcs.workshop.strategy.behav.NoFlyBehav
import ru.tcs.workshop.strategy.behav.NoRunBehav
import ru.tcs.workshop.strategy.behav.SquickBehav

class RubberDuck : Duck(
    flyBehavior = NoFlyBehav(),
    runBehavior = NoRunBehav(),
    eatBehav = NoEatBehav(),
    quackBehavior = SquickBehav()
) {
    override fun display() {
        println("[------]")
    }

    override fun swim() {
        println("<>")
    }
}
