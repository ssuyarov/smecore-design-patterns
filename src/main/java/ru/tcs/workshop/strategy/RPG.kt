package ru.tcs.workshop.strategy

abstract class Hero(
    private val fightBehavior: FightBehaviour
) {

    open fun fight() {
       fightBehavior.doFight()
    }
}

interface FightBehaviour {
    fun doFight()
}

class King : Hero(HeroFightBehaviour()) {
}

class HeroFightBehaviour : FightBehaviour {
    override fun doFight() {
        println("Like A Hero!")
    }
}

class Queen : Hero(HeroFightBehaviour()) {

}

class SVertuxiBehaviour : FightBehaviour {
    override fun doFight() {

        println("C Вертухи!") //To change body of created functions use File | Settings | File Templates.
    }
}

class Troll : Hero(SVertuxiBehaviour()) {
}

class Elf : Hero(SVertuxiBehaviour()) {
}
fun main() {
    listOf<Hero>(Elf(), Troll(), King(), Queen(), Troll(), Troll()).forEach {
        it.fight()
    }
}