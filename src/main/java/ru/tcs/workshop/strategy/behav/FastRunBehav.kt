package ru.tcs.workshop.strategy.behav

import ru.tcs.workshop.strategy.behav.RunBehavior

class FastRunBehav : RunBehavior {
    override fun run() {
        println(" ============>>>>>")
    }
}
