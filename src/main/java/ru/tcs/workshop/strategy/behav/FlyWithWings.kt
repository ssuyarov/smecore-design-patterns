package ru.tcs.workshop.strategy.behav

import ru.tcs.workshop.strategy.behav.FlyBehavior

class FlyWithWings : FlyBehavior {
    override fun fly() {
        println("\\_/")
    }
}
