package ru.tcs.workshop.strategy.behav

class SquickBehav : QuackBehavior {
    override fun quack() {
        println("Squeack")
    }
}
