package ru.tcs.workshop.strategy.behav

interface FlyBehavior {
    fun fly()
}

interface RunBehavior {
    fun run()
}

interface QuackBehavior {
    fun quack()
}
interface EatBehavior {
    fun eat()

}
