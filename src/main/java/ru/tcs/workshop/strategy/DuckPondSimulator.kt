package ru.tcs.workshop.strategy

import ru.tcs.workshop.strategy.duck.Duck

class DuckPondSimulator {
    fun simulate(ducks: List<Duck>) {
        ducks.forEach{ duck ->
            println("===============")
            duck.display()
            duck.quack()
            duck.swim()
            duck.fly()
            duck.run()
            duck.eat()
        }
    }

}
