package ru.tcs.workshop

import java.util.UUID


fun generateId() = "${UUID.randomUUID()}".replace("-", "")