package ru.tcs.workshop

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class ExtensionsKtTest {

    @Test
    fun `Check generateId is pretty`() {
        println(generateId())
        Assertions.assertFalse(generateId().contains('-'))
    }
}