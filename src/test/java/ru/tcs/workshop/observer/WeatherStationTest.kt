package ru.tcs.workshop.observer

import org.junit.jupiter.api.Assertions.*

import org.junit.jupiter.api.Test

internal class WeatherStationTest {

    @Test
    fun notifyObservers() {
        WeatherStation().updateMeasurements()
    }
}