package ru.tcs.workshop.observer

import org.junit.jupiter.api.Test

internal class ForecastDisplayTest {
    @Test
    fun display() {
        ForecastDisplay().display()
    }

    @Test
    fun update() {
        ForecastDisplay().update(80.0, 82.0, 10.0)
    }
}