package ru.tcs.workshop.observer

import org.junit.jupiter.api.Assertions.*

import org.junit.jupiter.api.Test

internal class StatisticsDisplayTest {

    @Test
    fun display() {
        StatisticsDisplay().display()
    }

    @Test
    fun update() {
        StatisticsDisplay().update(80.0, 82.0, 10.0)
    }
}