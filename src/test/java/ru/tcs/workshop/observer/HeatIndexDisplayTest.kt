package ru.tcs.workshop.observer

import org.junit.jupiter.api.Test

internal class HeatIndexDisplayTest {
    @Test
    fun display() {
        HeatIndexDisplay().display()
    }

    @Test
    fun update() {
        HeatIndexDisplay().update(80.0, 82.0, 10.0)
    }
}