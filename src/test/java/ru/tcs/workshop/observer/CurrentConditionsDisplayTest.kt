package ru.tcs.workshop.observer

import org.junit.jupiter.api.Assertions.*

import org.junit.jupiter.api.Test

internal class CurrentConditionsDisplayTest {

    @Test
    fun display() {
        CurrentConditionsDisplay().display()
    }

    @Test
    fun update() {
        CurrentConditionsDisplay().update(80.0, 82.0, 10.0)
    }
}