package ru.tcs.workshop.strategy

import org.junit.jupiter.api.Test
import ru.tcs.workshop.strategy.duck.AsianDuck
import ru.tcs.workshop.strategy.duck.DecoyDuck
import ru.tcs.workshop.strategy.duck.Duck
import ru.tcs.workshop.strategy.duck.MallardDuck
import ru.tcs.workshop.strategy.duck.RedheadDuck
import ru.tcs.workshop.strategy.duck.RubberDuck

internal class StrategyPatternTest {

    @Test
    fun `Check code compiles and runs without errors`() {
        DuckPondSimulator().simulate(
            listOf<Duck>(
                MallardDuck(),
                RedheadDuck(),
                RubberDuck(),
                DecoyDuck(),
                AsianDuck(),
                MallardDuck(),
                RedheadDuck(),
                RubberDuck(),
                DecoyDuck(),
                AsianDuck()
            )
        )
    }
}